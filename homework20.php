<form action="" method="post">
    <p>
        <lable for="username">Username:</lable>
        <input type="text" name="username" id="username">
    </p>
    <p>
        <lable for="secondname">Second name:</lable>
        <input type="text" name="secondname" id="secondname">
    </p>
    <p>
        <lable for="email">Email:</lable>
        <input type="email" name="email" id="email">
    </p>
    <div>
        <h4>Do you agree to the processing of data?</h4>
        <p>
            <label>
                <input type="checkbox" name="consent" value="1">
                Agree
            </label> 
          
        </p>
    </div>
    <button type="submit" name="submit">Submit</button>
</form>

<?php

function validate_text($fieldname){
    return isset($_POST[$fieldname]) && !empty(trim($_POST[$fieldname])) ? $_POST[$fieldname] : false;
}

function validate_email($fieldname){
    return filter_var($_POST[$fieldname], FILTER_VALIDATE_EMAIL) ? $_POST[$fieldname] : false;
}

function validate_consent($fieldname){
    return $_POST[$fieldname] !=='' ? $_POST[$fieldname] : false;
}

if ($_SERVER['REQUEST_METHOD']=='POST'){
    $username = validate_text('username');
    $secondname = validate_text('secondname');
    $email = validate_email('email');
    $consent = validate_consent('consent');

 $errors=[];
 
    if($username==false){
        $errors[]='username must be filled';
    }
 
    if($secondname==false){
        $errors[]='secondname must be filled';
    }
    
    if($email==false){
        $errors[]='email must be filled';
    }

    if($consent==false){
        $errors[]='consent must be noted';
    }

    if (!empty($errors)){
        foreach ($errors as $error){
            echo $error . '</br>';
        }
    }

}
